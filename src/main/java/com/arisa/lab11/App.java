package com.arisa.lab11;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        Bat bat1 = new Bat("Batman");
        bat1.eat();
        bat1.sleep();
        bat1.takeoff();
        bat1.fly();
        bat1.landing();
        Fish fish1 = new Fish("Nemo");
        fish1.eat();
        fish1.sleep();
        Plane plane1 = new Plane("Boing", "Boing Engine");
        plane1.takeoff();
        plane1.fly();
        plane1.landing();
        Crocodile crocodile1 = new Crocodile("Barbgon");
        crocodile1.crawl();
        crocodile1.eat();
        crocodile1.sleep();
        crocodile1.swim();
        Snake snake1 = new Snake("Black");
        snake1.crawl();
        snake1.eat();
        snake1.sleep();
        Bird bird1 = new Bird("Sep");
        bird1.eat();
        bird1.sleep();
        bird1.fly();
        bird1.takeoff();
        bird1.landing();
        bird1.run();
        bird1.walk();
        Submarine submarine1 = new Submarine("Ponyo", "Boing Engine");
        submarine1.swim();
        Cat cat1 = new Cat("Yuya");
        cat1.eat();
        cat1.sleep();
        cat1.run();
        cat1.walk();
        Flyable[] flyablesObjects = {bat1, plane1};
        for(int i=0; i<flyablesObjects.length;i++) {
            flyablesObjects[i].takeoff();
            flyablesObjects[i].fly();
            flyablesObjects[i].landing();
        }
    }
}
